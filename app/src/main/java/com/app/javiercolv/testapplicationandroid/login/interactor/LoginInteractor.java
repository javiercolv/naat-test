package com.app.javiercolv.testapplicationandroid.login.interactor;

import com.app.javiercolv.testapplicationandroid.login.Model.response.LoginResponse;

public interface LoginInteractor {
    void signIn(String username, String password);
    boolean validFields(String... fields);
    void onLoginResponse(LoginResponse loginResponse);
}
