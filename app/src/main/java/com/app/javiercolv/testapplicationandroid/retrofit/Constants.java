package com.app.javiercolv.testapplicationandroid.retrofit;

public class Constants {
    public static final String BASE_URL = "https://agentemovil.pagatodo.com";
    public static final String SERVICE_LOGIN_URL = "AgenteMovil.svc/agMov/login";
}
