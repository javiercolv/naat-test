package com.app.javiercolv.testapplicationandroid.sqlitedb;

/**
 * Created by Javier Cruz on 21/01/2019.
 */

public class ConstantsDB {
    public static final String COMP_CLARO = "CLARO";
    public static final String COMP_TUENTI = "TUENTI";
    public static final String COMP_ENTEL = "ENTEL";

    public static final String IC_CLARO = "ic_claro_foreground";
    public static final String IC_TUENTI = "ic_tuenti_foreground";
    public static final String IC_ENTEL = "ic_entel_foreground";

    public static final String FIELD_NAME = "name";
    public static final String FIELD_ICON = "icon";

    public static final String SERVICE_TA = "TIEMPO AIRE";
    public static final String SERVICE_MEGAS = "MEGAS";

    public static final String TAB_COMPANIES = "companies";
}
