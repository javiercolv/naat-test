package com.app.javiercolv.testapplicationandroid.mobiletopup.view;

import com.app.javiercolv.testapplicationandroid.login.Model.response.LoginResponse;

public interface DetailView {
    void showDialog(AlertDialog alertDialog);
    void showError(String error);
    void finishView();
}
