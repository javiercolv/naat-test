package com.app.javiercolv.testapplicationandroid.retrofit;

import com.app.javiercolv.testapplicationandroid.login.Model.request.DataRequest;
import com.app.javiercolv.testapplicationandroid.login.Model.response.LoginResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;

public interface LoginService {
    @POST(Constants.SERVICE_LOGIN_URL)
    Call<LoginResponse> getLoginResponse(@HeaderMap Map<String, String> headers, @Body DataRequest dataRequest);
}
