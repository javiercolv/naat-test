package com.app.javiercolv.testapplicationandroid.login.repository.local;

import com.app.javiercolv.testapplicationandroid.login.Model.response.LoginResponse;

public interface LoginRepositoryLocal {
    void saveLoginResponse(LoginResponse loginResponse);
    LoginResponse getSavedLoginResponse();
}
