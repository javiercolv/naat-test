package com.app.javiercolv.testapplicationandroid.mobiletopup.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Javier Cruz on 21/01/2019.
 */

public class CompaniesModel implements Parcelable{

    String name;
    String icon;

    public CompaniesModel(String name, String icon) {
        this.name = name;
        this.icon = icon;
    }

    protected CompaniesModel(Parcel in) {
        name = in.readString();
        icon = in.readString();
    }

    public static final Creator<CompaniesModel> CREATOR = new Creator<CompaniesModel>() {
        @Override
        public CompaniesModel createFromParcel(Parcel in) {
            return new CompaniesModel(in);
        }

        @Override
        public CompaniesModel[] newArray(int size) {
            return new CompaniesModel[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(icon);
    }
}
