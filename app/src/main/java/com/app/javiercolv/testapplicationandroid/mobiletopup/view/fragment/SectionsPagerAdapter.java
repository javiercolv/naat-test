package com.app.javiercolv.testapplicationandroid.mobiletopup.view.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.app.javiercolv.testapplicationandroid.mobiletopup.model.CompaniesModel;
import com.app.javiercolv.testapplicationandroid.sqlitedb.ConstantsDB;

import java.util.ArrayList;

/**
 * Created by Javier Cruz on 22/01/2019.
 */

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    String[] sections;
    public SectionsPagerAdapter(FragmentManager fm, String [] sections) {
        super(fm);
        this.sections = sections;
    }

    @Override
    public Fragment getItem(int position) {
        ArrayList<CompaniesModel> companies=new ArrayList<>();
        CompaniesModel companie1 = new CompaniesModel(ConstantsDB.COMP_CLARO,ConstantsDB.IC_CLARO);
        CompaniesModel companie2 = new CompaniesModel(ConstantsDB.COMP_ENTEL,ConstantsDB.IC_ENTEL);
        CompaniesModel companie3 = new CompaniesModel(ConstantsDB.COMP_TUENTI,ConstantsDB.IC_TUENTI);

        companies.add(companie1);
        companies.add(companie2);
        companies.add(companie3);

        return MenuPlaceholderFragment.newInstance(position + 1, companies);
    }

    @Override
    public int getCount() {
        return sections.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return sections[position];
    }
}

