package com.app.javiercolv.testapplicationandroid.mobiletopup.presenter;

import android.content.Context;

import com.app.javiercolv.testapplicationandroid.login.Model.response.LoginResponse;
import com.app.javiercolv.testapplicationandroid.mobiletopup.interactor.DetailInteractor;
import com.app.javiercolv.testapplicationandroid.mobiletopup.interactor.DetailInteractorImpl;
import com.app.javiercolv.testapplicationandroid.mobiletopup.view.AlertDialog;
import com.app.javiercolv.testapplicationandroid.mobiletopup.view.DetailView;

public class DetailPresenterImpl implements DetailPresenter {
    DetailView detailView;
    DetailInteractor detailInteractor;
    Context context;

    public DetailPresenterImpl(Context context, DetailView detailView) {
        this.detailView = detailView;
        this.detailInteractor = new DetailInteractorImpl(this, context);
        this.context = context;
    }

    @Override
    public void validData(String number, String amount) {
        detailInteractor.validData(number, amount);
    }

    @Override
    public void showDialog(LoginResponse loginResponse, String phoneNumber, String amount) {
        AlertDialog alertDialog = new AlertDialog(detailView, context, loginResponse, phoneNumber, amount);
        detailView.showDialog(alertDialog);
    }

    @Override
    public void showError(String error) {
        detailView.showError(error);
    }
}
