package com.app.javiercolv.testapplicationandroid.mobiletopup.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.javiercolv.testapplicationandroid.R;
import com.app.javiercolv.testapplicationandroid.mobiletopup.model.CompaniesModel;
import com.app.javiercolv.testapplicationandroid.mobiletopup.view.DetailActivity;
import com.app.javiercolv.testapplicationandroid.sqlitedb.ConstantsDB;

import java.util.ArrayList;

/**
 * Created by Javier Cruz on 22/01/2019.
 */

class AdapterCompanies extends RecyclerView.Adapter<AdapterCompanies.ViewHolderCompanies>{
    private ArrayList<CompaniesModel> arrayCompanies;
    private Context context;


    public AdapterCompanies(Context context, ArrayList<CompaniesModel> menu) {
        this.arrayCompanies = menu;
        this.context = context;
    }

    public class ViewHolderCompanies extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView imagen;
        public ImageView imagen2;
        public ImageView imagen3;
        public TextView nombre;
        int position;

        public ViewHolderCompanies(View v) {
            super(v);
            imagen = (ImageView) v.findViewById(R.id.iconCompanie);
            imagen2 = (ImageView) v.findViewById(R.id.iconCompanie2);
            imagen3 = (ImageView) v.findViewById(R.id.iconCompanie3);
            nombre=(TextView)v.findViewById(R.id.nameService);
            position = getAdapterPosition();
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            if(position != RecyclerView.NO_POSITION) {
                Log.i("ON_CLICK", arrayCompanies.get(position).getName());
                CompaniesModel companie = arrayCompanies.get(position);
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("companie", companie);

                context.startActivity(intent);
            }
        }
    }

    @Override
    public int getItemCount() {
        return arrayCompanies.size();
    }

    @Override
    public ViewHolderCompanies onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_companie, viewGroup, false);

        return new ViewHolderCompanies(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolderCompanies viewHolder, int i) {

        viewHolder.nombre.setText(arrayCompanies.get(i).getName());

        switch (arrayCompanies.get(i).getName()){
            case ConstantsDB.COMP_CLARO:
                viewHolder.imagen.setImageResource(R.mipmap.ic_claro_foreground);
                viewHolder.imagen2.setImageResource(R.mipmap.ic_claro_foreground);
                viewHolder.imagen3.setImageResource(R.mipmap.ic_claro_foreground);
                break;
            case ConstantsDB.COMP_ENTEL:
                viewHolder.imagen.setImageResource(R.mipmap.ic_entel_foreground);
                viewHolder.imagen2.setImageResource(R.mipmap.ic_entel_foreground);
                viewHolder.imagen3.setImageResource(R.mipmap.ic_entel_foreground);
                break;
            case ConstantsDB.COMP_TUENTI:
                viewHolder.imagen.setImageResource(R.mipmap.ic_tuenti_foreground);
                viewHolder.imagen2.setImageResource(R.mipmap.ic_tuenti_foreground);
                viewHolder.imagen3.setImageResource(R.mipmap.ic_tuenti_foreground);
                break;

        }

    }

}

