package com.app.javiercolv.testapplicationandroid.login.repository.remote;


import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.app.javiercolv.testapplicationandroid.login.Model.request.DataRequest;
import com.app.javiercolv.testapplicationandroid.login.Model.request.UserRequest;
import com.app.javiercolv.testapplicationandroid.login.Model.response.LoginResponse;
import com.app.javiercolv.testapplicationandroid.login.interactor.LoginInteractor;
import com.app.javiercolv.testapplicationandroid.retrofit.ApiClient;
import com.app.javiercolv.testapplicationandroid.retrofit.LoginService;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LoginRepositoryImpl implements LoginRepository {

    Context context;
    LoginInteractor loginInteractor;

    private static final String TAG = "LoginRepositoryImpl";

    public LoginRepositoryImpl(LoginInteractor loginInteractor, Context context) {
        this.context = context;
        this.loginInteractor = loginInteractor;
    }


    @Override
    public void getLoginResponse(String username, String password) {

        Call<LoginResponse> call = createCall(username, password);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                Log.i(TAG, response.toString());
                if (response.code()==200) {
                    loginInteractor.onLoginResponse(response.body());
                }
                else {
                    loginInteractor.onLoginResponse(null);
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                loginInteractor.onLoginResponse(null);
                Log.e(TAG, t.getMessage());
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private Call<LoginResponse> createCall(String user, String pass) {
        Retrofit retrofit = ApiClient.getApiClient();
        LoginService loginService = retrofit.create(LoginService.class);

        Map<String,String> headersMap = new HashMap();
        headersMap.put("SO","Android");
        headersMap.put("Version","2.5.2");

        UserRequest userRequest= new UserRequest(user, pass);

        DataRequest dataRequest = new DataRequest(userRequest);

        return loginService.getLoginResponse(headersMap, dataRequest);
    }
}
