package com.app.javiercolv.testapplicationandroid.login.Model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataRequest {
    @SerializedName("data")
    @Expose
    private UserRequest data;

    /**
     * No args constructor for use in serialization
     *
     */
    public DataRequest() {
    }

    /**
     *
     * @param data
     */
    public DataRequest(UserRequest data) {
        super();
        this.data = data;
    }

    public UserRequest getData() {
        return data;
    }

    public void setData(UserRequest data) {
        this.data = data;
    }
}
