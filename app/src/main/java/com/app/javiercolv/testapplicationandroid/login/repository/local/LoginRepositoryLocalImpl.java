package com.app.javiercolv.testapplicationandroid.login.repository.local;


import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.app.javiercolv.testapplicationandroid.login.Model.request.DataRequest;
import com.app.javiercolv.testapplicationandroid.login.Model.request.UserRequest;
import com.app.javiercolv.testapplicationandroid.login.Model.response.LoginResponse;
import com.app.javiercolv.testapplicationandroid.login.interactor.LoginInteractor;
import com.app.javiercolv.testapplicationandroid.retrofit.ApiClient;
import com.app.javiercolv.testapplicationandroid.retrofit.LoginService;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class LoginRepositoryLocalImpl implements LoginRepositoryLocal {

    Context context;
    LoginInteractor loginInteractor;

    private static final String TAG = "LoginRepositoryLocal";

    public LoginRepositoryLocalImpl(LoginInteractor loginInteractor, Context context) {
        this.context = context;
        this.loginInteractor = loginInteractor;
    }

    @Override
    public void saveLoginResponse(LoginResponse loginResponse) {
        Gson gson = new Gson();
        String loginResponseJsonObject= gson.toJson(loginResponse);
        SharedPreferences preferences = context.getSharedPreferences("SharedLoginResponse", context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("loginResponse", loginResponseJsonObject);
        editor.apply();
        Log.i("saveLoginResponse",loginResponseJsonObject);
    }

    @Override
    public LoginResponse getSavedLoginResponse() {
        Gson gson = new Gson();
        SharedPreferences preferences = context.getSharedPreferences("SharedLoginResponse", context.MODE_PRIVATE);
        String loginResponseJsonObject = preferences.getString("loginResponse","");
        Log.i("savedLogin", loginResponseJsonObject);
        if (loginResponseJsonObject!=null && !loginResponseJsonObject.equals(""))
            return gson.fromJson(loginResponseJsonObject, LoginResponse.class);
        else
            return new LoginResponse();
    }
}
