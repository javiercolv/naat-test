package com.app.javiercolv.testapplicationandroid.login.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.app.javiercolv.testapplicationandroid.R;
import com.app.javiercolv.testapplicationandroid.login.presenter.LoginPresenter;
import com.app.javiercolv.testapplicationandroid.login.presenter.LoginPresenterImpl;
import com.app.javiercolv.testapplicationandroid.mobiletopup.view.SectionsMenuActivity;

public class LoginActivity extends AppCompatActivity implements LoginView{

    private EditText txtUsername;
    private EditText txtPassword;
    private Button signInButton;
    private View mProgressView;
    private View mLoginFormView;

    private LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        txtUsername =  findViewById(R.id.txtUsername);
        txtPassword =  findViewById(R.id.txtPassword);

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        loginPresenter = new LoginPresenterImpl(this, getApplicationContext());

        signInButton = findViewById(R.id.btnSignIn);
        txtUsername.setText("Luis");
        txtPassword.setText("password");
        signInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = txtUsername.getText().toString();
                String password = txtPassword.getText().toString();
                loginPresenter.signIn(username, password);
            }
        });
    }

    @Override
    public void clearInputs() {
        txtUsername.setText("");
        txtPassword.setText("");
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void goSalesActivity() {
        Intent intentnt = new Intent(LoginActivity.this, SectionsMenuActivity.class);
        startActivity(intentnt);
    }

    @Override
    public void showLoginError(String error) {
        Toast.makeText(this,error,Toast.LENGTH_LONG).show();
    }
}

