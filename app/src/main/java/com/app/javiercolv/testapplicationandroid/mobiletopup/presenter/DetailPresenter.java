package com.app.javiercolv.testapplicationandroid.mobiletopup.presenter;

import com.app.javiercolv.testapplicationandroid.login.Model.response.LoginResponse;

public interface DetailPresenter {
    void validData(String number, String amount);
    void showDialog(LoginResponse loginResponse, String phoneNumber, String amount);
    void showError(String error);
}
