package com.app.javiercolv.testapplicationandroid.login.Model.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserRequest {

    @SerializedName("pass")
    @Expose
    private String pass;
    @SerializedName("user")
    @Expose
    private String user;

    public UserRequest(String user, String pass) {
        this.pass = pass;
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

}
