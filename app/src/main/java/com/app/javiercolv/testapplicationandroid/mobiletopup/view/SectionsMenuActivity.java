package com.app.javiercolv.testapplicationandroid.mobiletopup.view;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.app.javiercolv.testapplicationandroid.R;
import com.app.javiercolv.testapplicationandroid.mobiletopup.model.CompaniesModel;
import com.app.javiercolv.testapplicationandroid.mobiletopup.view.fragment.MenuPlaceholderFragment;
import com.app.javiercolv.testapplicationandroid.mobiletopup.view.fragment.SectionsPagerAdapter;
import com.app.javiercolv.testapplicationandroid.sqlitedb.ConstantsDB;

import java.util.ArrayList;

public class SectionsMenuActivity extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sales);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String [] sections = new String[]{
                "RECARGAS",
                "RECAUDACIÓN",
                "ADMINISTARCIÓN",
                "SECTION 4",
                "SECTION N"
        };
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), sections);

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = findViewById(R.id.tabLayout);


        tabLayout.removeAllTabs();
        for (String title: sections)
            tabLayout.addTab(tabLayout.newTab().setText(title));

        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_sales, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(this,"Ajustes",Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }
}
