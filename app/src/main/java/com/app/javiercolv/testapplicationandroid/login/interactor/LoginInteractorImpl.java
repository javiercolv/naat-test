package com.app.javiercolv.testapplicationandroid.login.interactor;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.util.Log;

import com.app.javiercolv.testapplicationandroid.R;
import com.app.javiercolv.testapplicationandroid.login.Model.response.LoginResponse;
import com.app.javiercolv.testapplicationandroid.login.presenter.LoginPresenter;
import com.app.javiercolv.testapplicationandroid.login.repository.local.LoginRepositoryLocal;
import com.app.javiercolv.testapplicationandroid.login.repository.local.LoginRepositoryLocalImpl;
import com.app.javiercolv.testapplicationandroid.login.repository.remote.LoginRepository;
import com.app.javiercolv.testapplicationandroid.login.repository.remote.LoginRepositoryImpl;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

public class LoginInteractorImpl implements LoginInteractor {

    private LoginPresenter loginPresenter;
    private LoginRepository loginRespository;
    private LoginRepositoryLocal loginRespositoryLocal;
    private Context context;

    private static final String TAG = "LoginInteractorImpl";


    public LoginInteractorImpl(LoginPresenter loginPresenter, Context context) {
        this.loginPresenter = loginPresenter;
        this.context = context;
        loginRespository = new LoginRepositoryImpl(this, this.context);
        loginRespositoryLocal = new LoginRepositoryLocalImpl(this, this.context);
    }

    @Override
    public void signIn(String username, String password) {
        LoginResponse savedLoginResponse = loginRespositoryLocal.getSavedLoginResponse();
        //TODO MANEJAR SAVED LOGIN RESPONSE
        if (!validFields(username,password)) {
            loginPresenter.loginError(context.getResources().getString(R.string.error_field_required));
            return;
        }
        loginRespository.getLoginResponse(username,password);
    }

    @Override
    public boolean validFields(String... fields) {
        for (String field : fields)
            if (field == null || field.equals(""))
                return false;
        return true;
    }

    @Override
    public void onLoginResponse(LoginResponse loginResponse) {
        if (loginResponse!=null) {
            if (loginResponse.getId_user() != null) {
                loginRespositoryLocal.saveLoginResponse(loginResponse);
                loginPresenter.loginSucces(loginResponse);
            } else if (loginResponse.getError() != null) {
                loginPresenter.loginError(loginResponse.getError().getMessage());
            } else {
                loginPresenter.loginError("Error");
            }
        }else {
            loginPresenter.loginError("Error");
        }


    }
}
