package com.app.javiercolv.testapplicationandroid.login.view;

public interface LoginView {
    void clearInputs();
    void showProgress(boolean show);
    void goSalesActivity();
    void showLoginError(String error);
}
