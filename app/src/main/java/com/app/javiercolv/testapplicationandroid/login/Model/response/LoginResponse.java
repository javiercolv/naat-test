package com.app.javiercolv.testapplicationandroid.login.Model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginResponse {
    @SerializedName("agente")
    @Expose
    private String agente;

    @SerializedName("error")
    @Expose
    private ErrorResponse error;

    @SerializedName("id_user")
    @Expose
    private Long id_user;

    @SerializedName("token")
    @Expose
    private String token;

    /**
     * No args constructor for use in serialization
     *
     */
    public LoginResponse() {
    }

    /**
     *
     * @param error
     * @param token
     * @param agente
     * @param id_user
     */
    public LoginResponse(String agente, ErrorResponse error, Long id_user, String token) {
        super();
        this.agente = agente;
        this.error = error;
        this.id_user = id_user;
        this.token = token;
    }

    public String getAgente() {
        return agente;
    }

    public void setAgente(String agente) {
        this.agente = agente;
    }

    public ErrorResponse getError() {
        return error;
    }

    public void setError(ErrorResponse error) {
        this.error = error;
    }

    public Long getId_user() {
        return id_user;
    }

    public void setId_user(Long id_user) {
        this.id_user = id_user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
