package com.app.javiercolv.testapplicationandroid.mobiletopup.view;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.app.javiercolv.testapplicationandroid.R;
import com.app.javiercolv.testapplicationandroid.login.Model.response.LoginResponse;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AlertDialog{
    private Context context;
    private Dialog dialog;
    public DetailView detailView;

    public AlertDialog(final DetailView detailView, final Context context, LoginResponse loginResponse, String telefono, String monto) {
        this.detailView = detailView;
        this.context = context;
        this.dialog = new Dialog(this.context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setContentView(R.layout.alertdialog_card_view);

        TextView txtIdUser = dialog.findViewById(R.id.txtIdUser);
        TextView txtTime = dialog.findViewById(R.id.txtTime);
        TextView txtDate = dialog.findViewById(R.id.txtDate);

        TextView txtNumber = dialog.findViewById(R.id.numeroTelefonico);
        TextView txtAmount = dialog.findViewById(R.id.montoARecargar);

        txtNumber.setText(telefono);
        txtAmount.setText("$"+new DecimalFormat("###.##").format(Double.parseDouble(monto)));

        txtIdUser.setText(String.valueOf(loginResponse.getId_user()));
        txtDate.setText(new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
        txtTime.setText(new SimpleDateFormat("h:mm a").format(new Date()));

        Button btnCancel = dialog.findViewById(R.id.btnCancel);
        Button btnAccept = dialog.findViewById(R.id.btnAccept);

        btnAccept.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //TODO Compra de tiempo aire, implementar Presenter e Intercator
                Toast.makeText(context, "Recarga realizada",Toast.LENGTH_SHORT).show();
                detailView.finishView();
                dialog.dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    public void showDialog(){
        dialog.show();
    }
}
