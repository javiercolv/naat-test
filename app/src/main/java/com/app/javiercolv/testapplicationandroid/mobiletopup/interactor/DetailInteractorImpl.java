package com.app.javiercolv.testapplicationandroid.mobiletopup.interactor;

import android.content.Context;

import com.app.javiercolv.testapplicationandroid.login.Model.response.LoginResponse;
import com.app.javiercolv.testapplicationandroid.mobiletopup.presenter.DetailPresenter;
import com.app.javiercolv.testapplicationandroid.mobiletopup.repository.DetailRepository;
import com.app.javiercolv.testapplicationandroid.mobiletopup.repository.DetailRepositoryImpl;
import com.app.javiercolv.testapplicationandroid.mobiletopup.view.AlertDialog;

public class DetailInteractorImpl implements DetailInteractor {
    DetailRepository detailRepository;
    DetailPresenter detailPresenter;
    Context context;

    public DetailInteractorImpl(DetailPresenter detailPresenter, Context context) {
        this.detailPresenter = detailPresenter;
        this.context = context;
        this.detailRepository = new DetailRepositoryImpl(this.context);

    }

    @Override
    public void validData(String number, String amount) {

        if (number.equals("") || number==null){
            detailPresenter.showError("Número de teléfono no válido");
            return;
        }else if (amount.equals("") || amount==null){
            detailPresenter.showError("Monto no válido");
            return;
        }else if (number.length()<10){
            detailPresenter.showError("El número de teléfono debe ser de 10 digitos");
            return;
        }else if (Integer.parseInt(amount) < 3 || Integer.parseInt(amount) > 100){
            detailPresenter.showError("El monto de la recarga debe ser entre $3.00 y $100.00");
            return;
        }

        LoginResponse loginResponse = detailRepository.getLoginResponse();
        if (loginResponse==null) {
            detailPresenter.showError("No fué posible recuperar usuario");
            return;
        }
        detailPresenter.showDialog(loginResponse, number, amount);

    }
}
