package com.app.javiercolv.testapplicationandroid.mobiletopup.repository;

import android.content.Context;

import com.app.javiercolv.testapplicationandroid.login.Model.response.LoginResponse;
import com.app.javiercolv.testapplicationandroid.login.repository.local.LoginRepositoryLocal;
import com.app.javiercolv.testapplicationandroid.login.repository.local.LoginRepositoryLocalImpl;

public class DetailRepositoryImpl implements DetailRepository {
    LoginRepositoryLocal loginRepository;
    Context context;

    public DetailRepositoryImpl(Context context){
        this.context = context;
        loginRepository = new LoginRepositoryLocalImpl(null, context);
    }
    @Override
    public LoginResponse getLoginResponse() {
        return loginRepository.getSavedLoginResponse();
    }
}
