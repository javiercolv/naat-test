package com.app.javiercolv.testapplicationandroid.mobiletopup.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.app.javiercolv.testapplicationandroid.R;
import com.app.javiercolv.testapplicationandroid.mobiletopup.model.CompaniesModel;

import java.util.ArrayList;

/**
 * Created by Javier Cruz on 21/01/2019.
 */

public class MenuPlaceholderFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    public static ArrayList<CompaniesModel> arrayCompanies;


    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager lManager;


    LayoutInflater inflater;

    Toolbar toolbar;

    Context context;

    public MenuPlaceholderFragment() {

    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static MenuPlaceholderFragment newInstance(int sectionNumber, ArrayList<CompaniesModel> arrayM) {
        MenuPlaceholderFragment fragment = new MenuPlaceholderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putParcelableArrayList(String.valueOf(arrayCompanies), arrayM);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_companies, container, false);
        // TextView textView = (TextView) rootView.findViewById(R.id.section_label);
        // textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));

        recycler = (RecyclerView) rootView.findViewById(R.id.recyclerCompanies);
        recycler.setHasFixedSize(true);

        this.inflater=inflater;

        // Usar un administrador para LinearLayout
        lManager = new LinearLayoutManager(inflater.getContext());
        recycler.setLayoutManager(lManager);

        adapter = new AdapterCompanies(inflater.getContext(), getArguments().<CompaniesModel>getParcelableArrayList(String.valueOf(arrayCompanies)));

        recycler.setAdapter(adapter);


        return rootView;
    }


}