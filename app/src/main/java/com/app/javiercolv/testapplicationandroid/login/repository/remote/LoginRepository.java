package com.app.javiercolv.testapplicationandroid.login.repository.remote;

import com.app.javiercolv.testapplicationandroid.login.Model.response.LoginResponse;

public interface LoginRepository {
    void getLoginResponse(String username, String password);
}
