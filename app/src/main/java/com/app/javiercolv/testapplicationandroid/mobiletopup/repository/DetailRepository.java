package com.app.javiercolv.testapplicationandroid.mobiletopup.repository;

import com.app.javiercolv.testapplicationandroid.login.Model.response.LoginResponse;

public interface DetailRepository {
    LoginResponse getLoginResponse();
}
