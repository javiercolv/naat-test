package com.app.javiercolv.testapplicationandroid.sqlitedb;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Javier Cruz on 21/01/2019.
 */

public class DBOpenHelper extends SQLiteOpenHelper {

    String createCompanies = "CREATE TABLE "+ConstantsDB.TAB_COMPANIES+" (" +
            ConstantsDB.FIELD_NAME +" varchar(20)," +
            ConstantsDB.FIELD_ICON +" varchar(20),"+
            "primary key("+ConstantsDB.FIELD_NAME+"));";

    public DBOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(createCompanies);
        ContentValues values = new ContentValues();
        values.put(ConstantsDB.FIELD_NAME, ConstantsDB.COMP_CLARO);
        values.put(ConstantsDB.FIELD_ICON, ConstantsDB.IC_CLARO);
        values.put(ConstantsDB.FIELD_NAME, ConstantsDB.COMP_TUENTI);
        values.put(ConstantsDB.FIELD_ICON, ConstantsDB.IC_TUENTI);
        values.put(ConstantsDB.FIELD_NAME, ConstantsDB.COMP_ENTEL);
        values.put(ConstantsDB.FIELD_ICON, ConstantsDB.IC_ENTEL);
        db.insert(ConstantsDB.TAB_COMPANIES, null, values);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("drop table "+ ConstantsDB.TAB_COMPANIES);
        db.execSQL(createCompanies);
        ContentValues values = new ContentValues();
        values.put(ConstantsDB.FIELD_NAME, ConstantsDB.COMP_CLARO);
        values.put(ConstantsDB.FIELD_ICON, ConstantsDB.IC_CLARO);
        values.put(ConstantsDB.FIELD_NAME, ConstantsDB.COMP_TUENTI);
        values.put(ConstantsDB.FIELD_ICON, ConstantsDB.IC_TUENTI);
        values.put(ConstantsDB.FIELD_NAME, ConstantsDB.COMP_ENTEL);
        values.put(ConstantsDB.FIELD_ICON, ConstantsDB.IC_ENTEL);
        db.insert(ConstantsDB.TAB_COMPANIES, null, values);
    }
}
