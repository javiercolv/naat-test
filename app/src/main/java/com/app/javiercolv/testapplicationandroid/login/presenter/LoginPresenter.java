package com.app.javiercolv.testapplicationandroid.login.presenter;

import com.app.javiercolv.testapplicationandroid.login.Model.response.LoginResponse;

public interface LoginPresenter {
    void signIn(String username, String password);//Interactor
    void loginSucces(LoginResponse userRequest);
    void loginError(String error);
}
