package com.app.javiercolv.testapplicationandroid.mobiletopup.interactor;

public interface DetailInteractor {
    void validData(String number, String amount);
}
