package com.app.javiercolv.testapplicationandroid.mobiletopup.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.app.javiercolv.testapplicationandroid.R;
import com.app.javiercolv.testapplicationandroid.login.Model.response.LoginResponse;
import com.app.javiercolv.testapplicationandroid.mobiletopup.model.CompaniesModel;
import com.app.javiercolv.testapplicationandroid.mobiletopup.presenter.DetailPresenter;
import com.app.javiercolv.testapplicationandroid.mobiletopup.presenter.DetailPresenterImpl;

public class DetailActivity extends AppCompatActivity implements DetailView {

    EditText editPhoneNumber, editAmount;
    Button btnBuyTA;

    DetailPresenter detailPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        CompaniesModel companie = getIntent().getParcelableExtra("companie");

        editAmount = findViewById(R.id.editAmount);
        editPhoneNumber = findViewById(R.id.editPhoneNumber);

        btnBuyTA = findViewById(R.id.btnBuyTA);

        detailPresenter = new DetailPresenterImpl(this, this);

        btnBuyTA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                detailPresenter.validData(editPhoneNumber.getText().toString(),
                        editAmount.getText().toString());
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
       // getSupportActionBar().setHomeButtonEnabled(true);

    }

    @Override
    public void showDialog(AlertDialog alertDialog) {
           alertDialog.showDialog();
    }

    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void finishView() {
        this.finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        finishView();
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
               finishView();
        }
        return super.onOptionsItemSelected(item);
    }
}
