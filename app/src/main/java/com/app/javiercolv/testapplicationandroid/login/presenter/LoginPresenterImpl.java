package com.app.javiercolv.testapplicationandroid.login.presenter;

import android.content.Context;

import com.app.javiercolv.testapplicationandroid.login.Model.response.LoginResponse;
import com.app.javiercolv.testapplicationandroid.login.interactor.LoginInteractor;
import com.app.javiercolv.testapplicationandroid.login.interactor.LoginInteractorImpl;
import com.app.javiercolv.testapplicationandroid.login.view.LoginView;

public class LoginPresenterImpl implements LoginPresenter {

    LoginView loginView;
    LoginInteractor loginInteractor;
    Context applicationContext;

    public LoginPresenterImpl(LoginView loginView, Context applicationContext) {
        this.loginView = loginView;
        this.applicationContext = applicationContext;
        loginInteractor = new LoginInteractorImpl(this,this.applicationContext);
    }

    @Override
    public void signIn(String username, String password) {
        loginView.showProgress(true);
        loginInteractor.signIn(username, password);

    }

    @Override
    public void loginSucces(LoginResponse loginResponse) {
        loginView.showProgress(false);
        loginView.clearInputs();
        loginView.goSalesActivity();

    }

    @Override
    public void loginError(String error) {
        loginView.showProgress(false);
        loginView.showLoginError(error);
    }
}
